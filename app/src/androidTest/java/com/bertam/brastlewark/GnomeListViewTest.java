package com.bertam.brastlewark;

import android.widget.EditText;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

import com.bertam.brastlewark.model.Gnome;
import com.bertam.brastlewark.viewModel.GnomeListViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;


public class GnomeListViewTest  {

    public static final String TEST_STRING = "cogwitz";
    private GnomeListViewModel viewModel = mock(GnomeListViewModel.class);
    private Gnome gnome = mock(Gnome.class);


    @Rule
    public final ActivityScenarioRule<MainActivity> mActivityRule = new ActivityScenarioRule<>(MainActivity.class);


    @Test
    public void testUI() {
        mActivityRule.getScenario().onActivity(activity -> {
            assertTrue(activity.findViewById(R.id.searchViewGnome).isShown());
        });
    }

    @Test
    public void testGnomeNotFoundVisibility() {
        onView(withId(R.id.searchViewGnome)).perform(click());
        onView(isAssignableFrom(EditText.class)).perform(typeText("Not found"), pressImeActionButton());
        onView(withId(R.id.emptyGnomeList)).check(matches(isDisplayed()));
        onView(withId(R.id.recyclerViewGnome)).check(matches(not(isDisplayed())));

    }
    @Test
    public void testGnomeFoundVisibility() {
        onView(withId(R.id.searchViewGnome)).perform(click());
        onView(isAssignableFrom(EditText.class)).perform(typeText(TEST_STRING), pressImeActionButton());

        onView(withId(R.id.emptyGnomeList)).check(matches(not(isDisplayed())));
        onView(withId(R.id.recyclerViewGnome)).check(matches(isDisplayed()));

    }

    @Test
    public void clickFilterButton() {
        onView(withId(R.id.filter_gnomes)).perform(click());

        onView(withText(R.string.select_filter))
                .inRoot(isDialog()) // <---
                .check(matches(isDisplayed()));

    }
    @Test
    public void checkClickListGnome(){
        onView(allOf(withId(R.id.recyclerViewGnome), isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
    }
}
