package com.bertam.brastlewark.viewModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.bertam.brastlewark.io.GnomeResponse;
import com.bertam.brastlewark.model.Gnome;
import com.bertam.brastlewark.repository.GnomeRepository;

import java.util.List;

public class GnomeViewModel extends AndroidViewModel{

    private GnomeRepository mRepository;


    public GnomeViewModel(Application application) {
        super(application);
        mRepository = GnomeRepository.getInstance(application);

    }


    public Gnome getSingleGnome(int id) {
        return mRepository.getGnomeById(id);
    }


}
