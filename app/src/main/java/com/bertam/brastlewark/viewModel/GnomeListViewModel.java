package com.bertam.brastlewark.viewModel;

import android.app.Application;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.bertam.brastlewark.model.Gnome;
import com.bertam.brastlewark.repository.GnomeRepository;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;


public class GnomeListViewModel extends AndroidViewModel {
    private final LiveData<List<Gnome>> gnomesFiltered;
    private final MutableLiveData<HashMap<String, String>> filterBy;


    public GnomeListViewModel(Application application) {
        super(application);

        GnomeRepository gnomeRepository = GnomeRepository.getInstance(application);
        gnomeRepository.getAllGnomes();

        filterBy = new MutableLiveData<>();

        gnomesFiltered = Transformations.switchMap(filterBy, gnomeRepository::searchBy);
    }


    public LiveData<List<Gnome>> getSearchByLiveData() {
        return gnomesFiltered;
    }


    public void setFilterBy(String input, String by) {


        if (filterBy.getValue() != null){
            if (Objects.equals(filterBy.getValue().get("search"), input)) {
                return;
            }
        }

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("search", input);
        hashMap.put("filter", by.toLowerCase());
        filterBy.setValue(hashMap);

    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        if (url != null) {
            Glide.with(imageView.getContext())
                    .load(url)
                    .dontAnimate()
                    .fitCenter()
                    .override(400, 300)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {

                            Log.e("testphoto", "onLoadFailed: " + e.getRootCauses());

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Object resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }

                    })
                    .into(imageView);
        }

    }


}

