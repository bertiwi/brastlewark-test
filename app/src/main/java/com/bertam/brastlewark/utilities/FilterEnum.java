package com.bertam.brastlewark.utilities;

public enum FilterEnum {
    NAME("Name"),
    AGE("Age"),
    HAIR("Hair");

    private String stringValue;
    private FilterEnum(String toString) {
        stringValue = toString;
    }

    @Override
    public String toString() {
        return stringValue;
    }
}
