package com.bertam.brastlewark.utilities;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.TypeConverters;

import com.bertam.brastlewark.dao.GnomeDao;
import com.bertam.brastlewark.model.Gnome;

@Database(entities = {Gnome.class}, version = 5, exportSchema = false)
@TypeConverters({Converter.class})

public abstract class RoomDatabase extends androidx.room.RoomDatabase {
    private static RoomDatabase INSTANCE;
    private static final String DATABASE_NAME = "room_database.db";
    public static final int DATABASE_VERSION = 1;

    public abstract GnomeDao gnomeDao();

    public static void setDatabaseNull() {
        INSTANCE = null;
    }

    public static RoomDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), RoomDatabase.class , DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }
}