package com.bertam.brastlewark.model;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.bertam.brastlewark.utilities.Converter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

@Entity
@TypeConverters(Converter.class)
public class Gnome {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name = "";
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail = "";
    @SerializedName("age")
    @Expose
    private int age = 0;
    @SerializedName("weight")
    @Expose
    private Double weight = 0.0;
    @SerializedName("height")
    @Expose
    private Double height = 0.0;
    @SerializedName("hair_color")
    @Expose
    private String hair_color = "";
    @SerializedName("professions")
    @Expose
    private ArrayList<String> professions;
    @SerializedName("friends")
    @Expose
    private ArrayList<String> friends;

    public Gnome(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getAge() {
        return age;
    }
    public String getAgeStr() {
        return String.valueOf(age);
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Double getWeight() {
        return weight;
    }
    public String getWeightStr() {
        return weight.toString();
    }


    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getHeight() {
        return height;
    }

    public String getHeightStr() {
        return height.toString();
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getHair_color() {
        return hair_color;
    }

    public void setHair_color(String hair_color) {
        this.hair_color = hair_color;
    }

    public ArrayList<String> getProfessions() {
        return professions;
    }

    public void setProfessions(ArrayList<String> professions) {
        this.professions = professions;
    }

    public ArrayList<String> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<String> friends) {
        this.friends = friends;
    }
}
