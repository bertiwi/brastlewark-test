package com.bertam.brastlewark;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.SearchView;

import com.bertam.brastlewark.databinding.ActivityMainBinding;
import com.bertam.brastlewark.io.GnomeResponse;
import com.bertam.brastlewark.model.Gnome;
import com.bertam.brastlewark.utilities.FilterEnum;
import com.bertam.brastlewark.view.adapter.GnomeAdapter;
import com.bertam.brastlewark.view.ui.DetailGnomeActivity;
import com.bertam.brastlewark.viewModel.GnomeListViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private GnomeAdapter gnomeAdapter;
    private ActivityMainBinding binding;
    private GnomeListViewModel viewModel;
    private int selectedPos = -1;
    private String filter = "Name";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setAdapter();
        setSearch();
        setFilterButton();

        viewModel = ViewModelProviders.of(this).get(GnomeListViewModel.class);
        observeViewModel(viewModel);

    }

    private void setFilterButton() {
        binding.filterGnomes.setOnClickListener(this::dialogFilterList);
    }


    private void setAdapter() {

        gnomeAdapter = new GnomeAdapter(gnome -> {
            //go to detail gnome
            Intent intent = new Intent(this, DetailGnomeActivity.class);
            intent.putExtra("id", gnome.getId());
            startActivity(intent);
        });

        binding.recyclerViewGnome.setAdapter(gnomeAdapter);
        binding.setIsLoading(true);
    }

    private void observeViewModel(GnomeListViewModel viewModel) {
        // ppdate the list when the data changes
        viewModel.getSearchByLiveData().observe(this, gnomes -> {
            if (gnomes != null) {
                binding.setIsLoading(false);

                if (gnomes.size() > 0) {
                    binding.recyclerViewGnome.setVisibility(View.VISIBLE);
                    binding.emptyGnomeList.setVisibility(View.GONE);
                } else {
                    binding.recyclerViewGnome.setVisibility(View.GONE);
                    binding.emptyGnomeList.setVisibility(View.VISIBLE);
                }

                gnomeAdapter.setGnomeList(gnomes);

            }
        });

        viewModel.setFilterBy("%", filter);


    }

    private void setSearch() {
        binding.searchViewGnome.setIconifiedByDefault(false);
        binding.searchViewGnome.setOnQueryTextListener(this);
        binding.searchViewGnome.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(binding.searchViewGnome.getWindowToken(), 0);
                return false;
            }
        });

        int searchCloseButtonId = binding.searchViewGnome.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButton = (ImageView) binding.searchViewGnome.findViewById(searchCloseButtonId);

        closeButton.setOnClickListener(v -> {
            binding.searchViewGnome.clearFocus();
            binding.searchViewGnome.setQuery("", false);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(binding.searchViewGnome.getWindowToken(), 0);

        });

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        viewModel.setFilterBy("%" + newText + "%", "name");
        return false;

    }

    public void dialogFilterList(View view) {

        final AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
        final ArrayList<String> arrayList = new ArrayList<>();

        for (FilterEnum filter :
                FilterEnum.values()) {
            arrayList.add(filter.name());
        }

        final CharSequence[] items = arrayList.toArray(new CharSequence[0]);

        if (selectedPos == -1) {
            adb.setSingleChoiceItems(items, 0, null);
        } else {
            adb.setSingleChoiceItems(items, selectedPos, null);
        }
        adb.setNegativeButton(R.string.cancel, (dialog, which) -> {
        });
        adb.setPositiveButton(R.string.select, (dialogInterface, i) -> {

            int selectedPosition = ((AlertDialog) dialogInterface).getListView().getCheckedItemPosition();
            filter = arrayList.get(selectedPosition);
            viewModel.setFilterBy("%", filter);

        });
        adb.setTitle(R.string.select_filter);
        adb.setCancelable(false);
        adb.show();

    }
}