package com.bertam.brastlewark.view.callback;

import com.bertam.brastlewark.model.Gnome;

public interface GnomeClickCallback {
    void onClick(Gnome gnome);

}
