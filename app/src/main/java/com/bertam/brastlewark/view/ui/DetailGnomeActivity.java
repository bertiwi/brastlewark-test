package com.bertam.brastlewark.view.ui;

import android.os.Bundle;

import com.bertam.brastlewark.databinding.ActivityDetailGnomeBinding;
import com.bertam.brastlewark.databinding.ActivityMainBinding;
import com.bertam.brastlewark.model.Gnome;
import com.bertam.brastlewark.viewModel.GnomeListViewModel;
import com.bertam.brastlewark.viewModel.GnomeViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.view.View;
import android.widget.ArrayAdapter;

import com.bertam.brastlewark.R;

public class DetailGnomeActivity extends AppCompatActivity {

    private ActivityDetailGnomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_gnome);

        if (getIntent().hasExtra("id")) {
            GnomeViewModel viewModel = ViewModelProviders.of(this).get(GnomeViewModel.class);
            int id = getIntent().getIntExtra("id", 0);
            Gnome gnome = viewModel.getSingleGnome(id);
            binding.setGnome(gnome);
            binding.executePendingBindings();
            setAdapter(gnome);
            setFriends(gnome);
        } else {
            //goback message error
        }
    }

    public void setAdapter(Gnome gnome) {
        if (gnome.getProfessions().size() > 0) {
            binding.detailGnomeProfessionsContainer.setVisibility(View.VISIBLE);
            ArrayAdapter<String> itemsAdapter =
                    new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, gnome.getProfessions());
            binding.detailGnomeProfessionsList.setAdapter(itemsAdapter);
        } else {
            binding.detailGnomeProfessionsContainer.setVisibility(View.GONE);
        }

    }

    public void setFriends(Gnome gnome) {
        if (gnome.getFriends().size() > 0) {
            binding.detailGnomeFriendsContainer.setVisibility(View.VISIBLE);
            binding.detailGnomeFriends.setText("(" + String.valueOf(gnome.getFriends().size()) + ")");
        } else {
            binding.detailGnomeFriendsContainer.setVisibility(View.GONE);
        }
    }
}