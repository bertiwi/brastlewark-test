package com.bertam.brastlewark.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bertam.brastlewark.R;
import com.bertam.brastlewark.databinding.ItemGnomeBinding;
import com.bertam.brastlewark.model.Gnome;
import com.bertam.brastlewark.view.callback.GnomeClickCallback;

import java.util.List;

public class GnomeAdapter extends RecyclerView.Adapter<GnomeAdapter.GnomeViewHolder> {

    List<? extends Gnome> gnomeList;

    @Nullable
    private final GnomeClickCallback GnomeClickCallback;

    public GnomeAdapter(@Nullable GnomeClickCallback GnomeClickCallback) {
        this.GnomeClickCallback = GnomeClickCallback;
    }

    public void setGnomeList(final List<? extends Gnome> gnomeList) {
        if (this.gnomeList == null) {
            this.gnomeList = gnomeList;
            notifyItemRangeInserted(0, gnomeList.size());
        }else{
            this.gnomeList = gnomeList;
            notifyDataSetChanged();
        }
    }

    @Override
    public GnomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemGnomeBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_gnome,
                        parent, false);

        binding.setCallback(GnomeClickCallback);


        return new GnomeViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(GnomeViewHolder holder, int position) {
        holder.binding.setGnome(gnomeList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return gnomeList == null ? 0 : gnomeList.size();
    }

    static class GnomeViewHolder extends RecyclerView.ViewHolder {

        final ItemGnomeBinding binding;

        public GnomeViewHolder(ItemGnomeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}
