package com.bertam.brastlewark.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.bertam.brastlewark.io.GnomeResponse;
import com.bertam.brastlewark.model.Gnome;

import java.util.List;

@Dao
public interface GnomeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertGnome (Gnome Gnome);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllGnomes(List<Gnome> gnomeList);
    @Query("SELECT * FROM Gnome")
    LiveData<List<Gnome>> getAllGnomes();
    @Query("SELECT * FROM Gnome WHERE id = :id")
    Gnome getSingleGnome(int id);
    @Query("SELECT * FROM Gnome WHERE name LIKE :search ORDER BY name")
    LiveData<List<Gnome>> searchGnome(String search);
    @Query("SELECT * FROM Gnome WHERE :by LIKE :search")
    LiveData<List<Gnome>> filterGnome(String search, String by);
    @Query("SELECT * FROM Gnome WHERE age LIKE :search ORDER BY age DESC")
    LiveData<List<Gnome>> filterAge(String search);
    @Query("SELECT * FROM Gnome WHERE hair_color LIKE :search ORDER BY hair_color")
    LiveData<List<Gnome>> filterHair(String search);
    @Query("SELECT EXISTS(SELECT * FROM Gnome)")
    boolean isDataExists();
    @Update
    void updateGnome (Gnome Gnome);
    @Query("DELETE FROM Gnome")
    void deleteAll();
    @Delete
    void deleteGnome (Gnome Gnome);
}
