package com.bertam.brastlewark.io;

import com.bertam.brastlewark.model.Gnome;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GnomeResponse {

    @SerializedName("Brastlewark")
    @Expose
    private List<Gnome> gnomeList = null;

    public List<Gnome> getGnomeList() {
        return gnomeList;
    }

    public List<Gnome> setGnomeList(List<Gnome> list) {
        gnomeList = list;
        return gnomeList;
    }
}
