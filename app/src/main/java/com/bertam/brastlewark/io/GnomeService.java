package com.bertam.brastlewark.io;

import com.bertam.brastlewark.model.Gnome;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GnomeService {
    String API_GNOMES_URL = "https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json";


    @GET(API_GNOMES_URL)
    Call<GnomeResponse> downloadGnomes();

}
