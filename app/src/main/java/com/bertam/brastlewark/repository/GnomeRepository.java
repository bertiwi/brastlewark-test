package com.bertam.brastlewark.repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.bertam.brastlewark.dao.GnomeDao;
import com.bertam.brastlewark.io.APIClient;
import com.bertam.brastlewark.io.GnomeResponse;
import com.bertam.brastlewark.io.GnomeService;
import com.bertam.brastlewark.model.Gnome;
import com.bertam.brastlewark.utilities.RoomDatabase;


import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GnomeRepository {

    private final GnomeService gnomeService;
    private static GnomeRepository gnomeRepository;
    private MutableLiveData<List<Gnome>> gnomeLiveData;
    private final GnomeDao gnomeDao;


    private GnomeRepository(Application application) {

        RoomDatabase db = RoomDatabase.getDatabase(application);
        gnomeDao = db.gnomeDao();
        //TODO this gitHubService instance will be injected using Dagger in part #2 ...
        gnomeService = APIClient.getClient().create(GnomeService.class);

    }

    public synchronized static GnomeRepository getInstance(Application application) {
        //TODO No need to implement this singleton in Part #2 since Dagger will handle it ...
        if (gnomeRepository == null) {
            gnomeRepository = new GnomeRepository(application);
        }
        return gnomeRepository;
    }

    public LiveData<List<Gnome>> searchBy(HashMap<String, String> hashMap) {
        if (Objects.equals(hashMap.get("filter"), "age")){
            return gnomeDao.filterAge(hashMap.get("search"));

        } else if (Objects.equals(hashMap.get("filter"), "hair")){
            return gnomeDao.filterHair("%" + hashMap.get("search"));
        } else{
            return gnomeDao.searchGnome("%" + hashMap.get("search") + "%");
        }
    }

    public LiveData<List<Gnome>> search(String search) {

        return gnomeDao.searchGnome("%" + search + "%");
    }

    public Gnome getGnomeById(int id) {
        return gnomeDao.getSingleGnome(id);
    }

    public void getAllGnomes() {
        gnomeLiveData = new MutableLiveData<>();

        Observable.fromCallable(() -> {
            gnomeService.downloadGnomes().enqueue(new Callback<GnomeResponse>() {
                @Override
                public void onResponse(Call<GnomeResponse> call, Response<GnomeResponse> response) {


                    if (response.body() != null) {
                        //this update the ui
                        gnomeLiveData.postValue(response.body().getGnomeList());
                        gnomeDao.deleteAll();
                        gnomeDao.insertAllGnomes(response.body().getGnomeList());

                    }
                }

                @Override
                public void onFailure(Call<GnomeResponse> call, Throwable t) {

                    if (gnomeDao.isDataExists()) {

                        gnomeLiveData.postValue(gnomeDao.getAllGnomes().getValue());
                    } else {
                        gnomeLiveData.postValue(null);
                    }

                }
            });
            return false;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {
                    //Use result for something


                });


    }

}
