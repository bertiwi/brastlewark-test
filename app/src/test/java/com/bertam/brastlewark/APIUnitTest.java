package com.bertam.brastlewark;

import com.bertam.brastlewark.io.GnomeService;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;

public class APIUnitTest {
    private GnomeService gnomeService;


    @Before
    public void createService() {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

        gnomeService = new Retrofit.Builder()
                .baseUrl(GnomeService.API_GNOMES_URL + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient.build())
                .build()
                .create(GnomeService.class);
    }


    @Test
    public void getGnomeList() {
        try {
            Response response = gnomeService.downloadGnomes().execute();
            assertEquals(response.code(), 200);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
