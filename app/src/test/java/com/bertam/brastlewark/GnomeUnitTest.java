package com.bertam.brastlewark;

import com.bertam.brastlewark.model.Gnome;

import org.junit.Test;

import static org.junit.Assert.*;


public class GnomeUnitTest {

    @Test
    public void testId(){
        Gnome gnome = new Gnome();
        gnome.setId(1000);
        assertEquals(gnome.getId(), 1000);
    }

    @Test
    public void testName(){
        Gnome gnome = new Gnome();
        gnome.setName("test");
        assertEquals(gnome.getName(), "test");
    }

    @Test
    public void testPhoto(){
        Gnome gnome = new Gnome();
        gnome.setThumbnail("test");
        assertEquals(gnome.getThumbnail(), "test");
    }

    @Test
    public void testAge(){
        Gnome gnome = new Gnome();
        gnome.setAge(2);
        assertEquals(gnome.getAge(), 2);
    }

    @Test
    public void testHeight(){
        Gnome gnome = new Gnome();
        gnome.setHeight(1.7);
        assertEquals(1.7, gnome.getHeight(), 0.1);
    }
}
